<?php
  include("includes/header.php");
  include("includes/navbar.php");

  if (isset($_POST["edit_btn"])) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/scripts/employees.php";

    switch ($_POST["edit_btn"]) {
      case "account_settings":
        $data = array('edit_account_settings' => '', 'email' => $_POST["email"], 'password' => $_POST["password"], 'id' => $_GET["id"]);

        $options = array(
          'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
          )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { echo "Error"; }

        print_r($result);
        break;
    }
  }

  function infoElement($name, $placeholder) {
    if (!isset($name)) return;

    require "scripts/databases/db_connect.php";

    $sql = "SELECT $name FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_assoc($result);
      $element = "
      <tr>
        <td>$placeholder</td>
        <td>
          $row[$name]
        </td>
      </tr>
      ";
    }
    else {
      $element = "
      <tr>
        <td>$placeholder</td>
        <td>
          -
        </td>
      </tr>
      ";
    }
    
    return $element;
  }
?>
<div class="row mb-4">
  <div class="col-md-3 mb-md-0 mb-4">
    <div class="card">
      <div class="card-header pb-0">
        <img class="img-fluid mb-3" src="
        <?php
          $sql = "SELECT img FROM hrms_accounts WHERE ID='$_GET[id]'";
          $result = mysqli_query($conn, $sql);

          if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            echo "$row[img]";
          }
          else echo "Error";
        ?>
        " alt="ID-<?= $_GET["id"] ?>-Profile-Pic" style="border-radius:100%;">
        <h5>
        <?php
          $sql = "SELECT prefix, first_name, last_name FROM hrms_accounts WHERE ID='$_GET[id]'";
          $result = mysqli_query($conn, $sql);

          if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            echo "$row[prefix]$row[first_name] $row[last_name]";
          }
          else echo "-";
        ?>
        </h5>
        <span class="badge badge-sm bg-gradient-success">Onboarding</span>
        <hr>
      </div>
      <div class="card-body pt-0 pb-3">
        <div class="row">
          <div class="col-12">
            <i class="fa-solid fa-envelope me-2"></i>
            <span class="text-sm">
              <?php
                $sql = "SELECT email FROM hrms_accounts WHERE ID='$_GET[id]'";
                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                  $row = mysqli_fetch_assoc($result);
                  echo "$row[email]";
                }
                else echo "-";
              ?>
            </span>
          </div>
          <div class="col-12 mt-1">
            <i class="fa-solid fa-phone me-2"></i>
            <span class="text-sm">
              <?php
                $sql = "SELECT phone_tel FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                  $row = mysqli_fetch_assoc($result);
                  echo "$row[phone_tel]";
                }
                else echo "-";
                ?>
              </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="card mb-3">
      <div class="px-3 card-header p-0">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <form action="" method="get">
              <input type="hidden" name="id" value="<?= $_GET["id"] ?>">
              <button class="nav-link <?php if (!isset($_GET["tab"])) echo "active"; ?>" type="submit">General</button>
            </form>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if (isset($_GET["tab"]) && $_GET["tab"] === "job") echo "active"; ?>" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&tab=job"; ?>">Job</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if (isset($_GET["tab"]) && $_GET["tab"] === "account") echo "active"; ?>" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&tab=account"; ?>">Account Settings</a>
          </li>
        </ul>
      </div>
    </div>
    <?php
    if (isset($_GET["tab"])) {
      switch ($_GET["tab"]) {
        case "account":
          ?>
          <div class="card mb-3" id="">
            <div class="px-3 card-body p-0">
              <div class="row">
                <div class="col-12 py-2 pt-3 px-4 d-flex justify-content-between align-items-center">
                  <h5 class="d-inline-block"><i class="fa-solid fa-user me-2"></i> Account Settings</h5>
                  <button type="button" class="btn btn-outline-success py-2 px-4" data-bs-toggle="modal" data-bs-target="#accountSettings">
                    EDIT
                  </button>
                </div>
                <div class="col-12 pb-3">
                  <div class="row">
                    <div class="col-12 col-md-6">
                      <table class="table table-borderless text-sm">
                        <tbody>
                          <tr>
                            <td>Email</td>
                            <td>
                              <?php
                              $sql = "SELECT email FROM hrms_accounts WHERE ID='$_GET[id]'";
                              $result = mysqli_query($conn, $sql);

                              if (mysqli_num_rows($result) > 0) {
                                $row = mysqli_fetch_assoc($result);
                                echo "$row[email]";
                              }
                              else echo "-";
                              ?>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
          break;
        case "job":
          break;
      }
    }
    else {
      ?>
    <div class="card mb-3" id="">
      <div class="px-3 card-body p-0">
        <div class="row">
          <div class="col-12 py-2 pt-3 px-3 d-flex justify-content-between align-items-center">
            <h5 class="d-inline-block"><i class="fa-solid fa-address-card me-2"></i> Personal Info</h5>
            <button type="button" class="btn btn-outline-success py-2 px-4" data-bs-toggle="modal" data-bs-target="#personalInfo">
              EDIT
            </button>
          </div>
          <div class="col-12 pb-3">
            <div class="row">
              <div class="col-12 col-md-6">
                <table class="table table-borderless text-sm">
                  <tbody>
                    <tr>
                      <td>Full Name (TH)</td>
                      <td>
                        <?php
                        $sql = "SELECT prefix, first_name, last_name FROM hrms_accounts WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[prefix]$row[first_name] $row[last_name]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <?php
                      echo infoElement("sex", "Sex");
                    ?>
                    <tr>
                      <td>Nationality</td>
                      <td>
                        <?php
                        $sql = "SELECT nationality FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[nationality]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Religion</td>
                      <td>
                        <?php
                        $sql = "SELECT religion FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[religion]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Height</td>
                      <td>
                        <?php
                        $sql = "SELECT height FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[height]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-12 col-md-6">
                <table class="table table-borderless text-sm">
                  <tbody>
                    <tr>
                      <td>Full Name (EN)</td>
                      <td>
                        <?php
                        $sql = "SELECT prefix_en, firstname_en, lastname_en FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[prefix_en]$row[firstname_en] $row[lastname_en]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Date Of Birth</td>
                      <td>
                        <?php
                        $sql = "SELECT dob FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[dob]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Race</td>
                      <td>
                        <?php
                        $sql = "SELECT race FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[race]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Age</td>
                      <td>
                        <?php
                        $sql = "SELECT age FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[age]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Weight</td>
                      <td>
                        <?php
                        $sql = "SELECT height FROM hrms_accounts_personal_info WHERE ID='$_GET[id]'";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                          $row = mysqli_fetch_assoc($result);
                          echo "$row[height]";
                        }
                        else echo "-";
                        ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
      }
    ?>
  </div>
</div>

<div class="modal fade" id="accountSettings" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Account Settings</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>
      <div class="modal-body">
        <form method="POST">
          <div class="mb-3">
            <h6>Email</h6>
            <input name="email" value="
            <?php
            $sql = "SELECT email FROM hrms_accounts WHERE ID='$_GET[id]'";
            $result = mysqli_query($conn, $sql);

            if (mysqli_num_rows($result) > 0) {
              $row = mysqli_fetch_assoc($result);
              echo "$row[email]";
            }
            ?>
            " type="email" placeholder="Email" class="form-control">
          </div>
          <div class="mb-3">
            <h6>Password</h6>
            <input name="password" type="password" placeholder="Password" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" name="edit_btn" value="account_settings">Save / Close</button>
        </form>
      </div>
    </div>
  </div>
</div>

<?php
  include("includes/settings.php");
  include("includes/footer.php");
?>
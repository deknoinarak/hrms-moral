      <footer class="footer pt-3 pb-3">
        <div class="container-fluid">
          <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
              <div class="copyright text-center text-sm text-muted text-lg-start">
                &copy; <script>
                  document.write(new Date().getFullYear())
                </script>,
                Themes By
                <a href="https://www.creative-tim.com" class="font-weight-bold" target="_blank">Creative Tim</a>
                Ideas By
                <a href="https://www.moralcenter.or.th" class="font-weight-bold" target="_blank">Moral IT</a>
                Source Codes In
                <a href="https://github.com/Deknoinarak/hrms-moral" class="font-weight-bold" target="_blank">Github</a>
              </div>
            </div>
            <div class="col-lg-6">
              <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                <li class="nav-item">
                  <a href="https://www.moralcenter.or.th" class="nav-link text-muted" target="_blank">Moralcenter</a>
                </li>
                <li class="nav-item">
                  <a href="https://www.moralcenter.or.th/eservice" class="nav-link text-muted" target="_blank">E-service</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </main>

  <!--   Core JS Files   -->
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="./assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script src="./assets/js/plugins/chartjs.min.js"></script>
  <!-- Smooth Scrolling -->
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/argon-dashboard.js"></script>
  <script>darkMode(true)</script>
</body>

</html>

<?php
  mysqli_close($conn);
?>
<?php
  include("includes/header.php");
?>
<main class="main-content  mt-0">
  <section>
    <div class="page-header min-vh-100">
      <div class="container">
        <div class="row">
          <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
            <div class="card card-plain">
              <div class="card-header pb-0 text-start">
                <h4 class="font-weight-bolder">Sign In</h4>
                <p class="mb-0">Login To Moral HRMS</p>
              </div>
              <div class="card-body">
                <form method="POST">
                  <div class="mb-3">
                    <input name="email" type="email" class="form-control form-control-lg" placeholder="Email" required>
                  </div>
                  <div class="mb-3">
                    <input name="password" type="password" class="form-control form-control-lg" placeholder="Password" required>
                  </div>
                  <div class="text-center">
                    <button type="submit" name="login_btn" class="btn btn-lg btn-primary btn-lg w-100 mt-4 mb-0">Sign in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
            <div class="position-relative bg-gradient-warning h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center overflow-hidden" style="
                        background-image: url('https://moralcenter.or.th/images/News-PR-2565/20-05-65/1/S__47874082.jpg');
                        background-size: cover; background-repeat: no-repeat; background-position: center;">
              <span class="mask bg-gradient-primary opacity-4"></span>
              <h2 class="mt-5 text-white font-weight-bolder position-relative">"คนดีมีพื้นที่ยืน ความดีมีพื้นที่ในสังคม"</h2>
              <!-- <p class="text-white position-relative">The more effortless the writing looks, the more effort the writer actually put into the process.</p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<?php
  if (isset($_POST["login_btn"])) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/scripts/login.php";
    $data = array('login' => '', 'email' => $_POST["email"], 'password' => $_POST["password"]);

    // use key 'http' even if you send the request to https://...
    $options = array(
      'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
      )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { echo "Unsuccessful"; }

    print_r($result);
  }

  include("includes/footer.php");
?>
<?php
  include("includes/header.php");
  include("includes/navbar.php");
?>
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <div class="card-header pb-0">
        <h4>All Employee</h4>
        <div class="row">
          <div class="col-sm-12 d-flex">
            <a href="manage_user_add.php" class="btn btn-primary bg-gradient-primary me-3 disabled">
              <i class="fa-solid fa-magnifying-glass"></i>
              <span>Search</span>
            </a>
            <button type="button" class="btn btn-success bg-gradient-success me-3" data-bs-toggle="modal" data-bs-target="#createNewEmployee">
              <i class="fa-solid fa-plus text-lg pe-1"></i>
              <span>Add User</span>
            </button>
            <?php if (isset($_SESSION["error"])) echo $_SESSION["error"]; ?>
            <?php if (isset($_SESSION["success"])) echo $_SESSION["success"]; ?>
            <?php unset($_SESSION["success"]); unset($_SESSION["error"]); ?>
          </div>
          <div class="col-sm-12">
            
          </div>
        </div>
      </div>
      <div class="card-body px-0 pt-0 pb-2">
        <div class="table-responsive p-0">
          <table class="table align-items-center mb-0">
            <thead>
              <tr>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">#</th>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Name</th>
                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $sql = "SELECT ID, img, prefix, first_name, last_name, email FROM hrms_accounts";
              $result = mysqli_query($conn, $sql);

              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
              ?>
              <tr>
                <td class="align-middle text-center">
                  <span class="text-sm font-weight-bold"><?= $row["ID"] ?></span>
                </td>
                <td>
                  <a class="d-flex px-2 py-1" href="employee_info.php?id=<?= $row["ID"] ?>">
                    <div>
                      <img src="<?= $row["img"] ?>" class="avatar avatar-sm me-3" alt="user1">
                    </div>
                    <div class="d-flex flex-column justify-content-center">
                      <h6 class="mb-0 text-sm"><?= $row["prefix"] . $row["first_name"] . " " . $row["last_name"] ?></h6>
                      <p class="text-xs text-secondary mb-0"><?= $row["email"] ?></p>
                    </div>
                  </a>
                </td>
                <!-- <td class="align-middle text-center text-sm">
                  <span class="text-secondary text-xs font-weight-bold">0993490999</span>
                </td> -->
                <td class="align-middle text-center text-sm">
                  <span class="badge badge-sm bg-gradient-success">Onboarding</span>
                </td>
                <!-- <td class="align-middle text-center text-md">
                  <input type="hidden" name="">
                  <button type="submit" class="btn btn-success badge badge-sm bg-gradient-success my-1 py-2 px-3">More Info <i class="fa fa-chevron-right ps-1"></i></button>
                </td> -->
              </tr>
              <?php
                }
              } else {
              ?>
              <tr>
                <td>Not Found.</td>
              </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="createNewEmployee" tabindex="-999">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Profile</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="scripts/employees.php" method="post">
          <div class="mb-3">
            <div class="row g-3 align-items-center">
              <div class="col-md-2">
                <input type="text" class="form-control" name="name_prefix" placeholder="Prefix" required>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" name="name_first" placeholder="First Name" required>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" name="name_last" placeholder="Last Name" required>
              </div>
            </div>
          </div>
          <div class="mb-3">
            <input type="email" class="form-control" name="email" placeholder="Email" required>
          </div>
          <!-- <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="inviteLink">
            <label class="form-check-label" for="inviteLink">Send Invitation Link</label>
          </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" name="create_employee" class="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  const phoneInputField = document.querySelector("#phone");
  const phoneInput = window.intlTelInput(phoneInputField, {
    utilsScript:
      "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
  });
</script>

<?php
  include("includes/settings.php");
  include("includes/footer.php");
?>
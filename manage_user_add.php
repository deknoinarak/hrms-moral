<?php
  include("includes/header.php");
  include("includes/navbar.php");

  function getValues($name) {
    if (!isset($_POST[$name]))
      return null;
    
    return $_POST[$name];
  }

  // Elements
  function element($name) {
    switch ($name) {
      case "basics":
?>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12 mt-0"><h5>Employee Name (TH) <sup class="text-danger">*</sup></h5></div>
    <div class="col-md-2 mt-0">
      <input type="text" class="form-control" placeholder="Prefix (นาย/นาง/นางสาว)" name="name_prefix_th" value="<?= getValues("name_prefix_th") ?>" required>
    </div>
    <div class="col-md-5 mt-0">
      <input type="text" class="form-control" placeholder="First Name (สมหมาย)" name="name_first_th" value="<?= getValues("name_prefix_en") ?>" required>
    </div>
    <div class="col-md-5 mt-0">
      <input type="text" class="form-control" placeholder="Last Name (ร่ำรวย)" name="name_last_th" value="<?= getValues("name_last_th") ?>" required>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12"><h5>Employee Name (EN) <sup class="text-danger">*</sup></h5></div>
    <div class="col-md-2 mt-0">
      <input type="text" class="form-control" placeholder="Prefix (Mr./Mrs./Miss)" name="name_prefix_en" value="<?= getValues("name_prefix_en") ?>" required>
    </div>
    <div class="col-md-5 mt-0">
      <input type="text" class="form-control" placeholder="First Name (Sommaye)" name="name_first_en" value="<?= getValues("name_first_en") ?>" required>
    </div>
    <div class="col-md-5 mt-0">
      <input type="text" class="form-control" placeholder="Last Name (Ramrouy)" name="name_last_en" value="<?= getValues("name_last_en") ?>" required>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-md-6">
      <h5>Sex <sup class="text-danger">*</sup></h5>
      <select name="sex" class="form-select">
        <option value="male" <?php if (getValues("sex") === "male") { ?>selected<?php } ?>>Male</option>
        <option value="female" <?php if (getValues("sex") === "female") { ?>selected<?php } ?>>Female</option>
      </select>
    </div>
    <div class="col-md-6">
      <h5>Date Of Birth <sup class="text-danger">*</sup></h5>
      <input type="date" name="dob" id="dob" class="form-control" value="<?= getValues("dob") ?>" placeholder="Date Of Birth" required>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-md-4">
      <h5>Nationality <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Nationality (ไทย)" name="nationality" value="<?= getValues("nationality") ?>" required>
    </div>
    <div class="col-md-4">
      <h5>Race <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Race (ไทย)" name="race" value="<?= getValues("race") ?>" required>
    </div>
    <div class="col-md-4">
      <h5>Religion <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Religion (พุทธ)" name="religion" value="<?= getValues("religion") ?>" required>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-md-3">
      <h5>Age <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Age (12)" name="age" value="<?= getValues("age") ?>" required>
    </div>
    <div class="col-md-3">
      <h5>Height <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Height (123 ช.ม.)" name="height" value="<?= getValues("height") ?>" required>
    </div>
    <div class="col-md-3">
      <h5>Weight <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Weight (50 กก.)" name="weight" value="<?= getValues("weight") ?>" required>
    </div>
    <div class="col-md-3">
      <h5>Blood Group <sup class="text-danger">*</sup></h5>
      <input type="text" class="form-control" placeholder="Blood Group (A)" name="blood_group" value="<?= getValues("blood_group") ?>" required>
    </div>
  </div>
</div>
<div class="mb-3">
  <h5>Place Of Birth <sup class="text-danger">*</sup></h5>
  <input type="text" class="form-control" placeholder="Place Of Birth (ร.พ.บางอย่าง)" name="pob" value="<?= getValues("pob") ?>" required>
</div>
<h5 class="mt-5">Telephone Details</h5>
<hr>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-md-4">
      <h5>Home Telephone <sup class="text-danger">*</sup></h5>
      <input type="tel" class="form-control" placeholder="Home Telephone (+66 123456789)" name="home_tel" value="<?= getValues("home_tel") ?>" required>
    </div>
    <div class="col-md-4">
      <h5>Office Telephone</h5>
      <input type="tel" class="form-control" placeholder="Office Telephone (+66 123456789)" name="office_tel" value="<?= getValues("office_tel") ?>">
    </div>
    <div class="col-md-4">
      <h5>Mobile Telephone <sup class="text-danger">*</sup></h5>
      <input type="tel" class="form-control" placeholder="Mobile Telephone (+66 123456789)" name="mobile_tel" value="<?= getValues("mobile_tel") ?>" required>
    </div>
  </div>
</div>
<h5 class="mt-5">Address Details</h5>
<hr>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12"><h5>Current Address (TH) <sup class="text-danger">*</sup></h5></div>
    <div class="col-12 mt-0">
      <input type="text" name="current_address_th" class="form-control" value="<?= getValues("current_address_th") ?>" placeholder="Current Address (25/55 คอนโดแห่งหนี่ง ชั้น 7 ตึก 1...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_province_th" class="form-control" value="<?= getValues("current_address_province_th") ?>" placeholder="Province (ปทุมธานี/กรุงเทพฯ...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_district_th" class="form-control" value="<?= getValues("current_address_district_th") ?>" placeholder="District (สามโคก/พญาไท...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_subdistrict_th" class="form-control" value="<?= getValues("current_address_subdistrict_th") ?>" placeholder="Subdistrict (บางกระบือ/สามเสนใน...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_postcode_th" class="form-control" value="<?= getValues("current_address_postcode_th") ?>" placeholder="Postal Code (12345...)" required/>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12"><h5>Current Address (EN) <sup class="text-danger">*</sup></h5></div>
    <div class="col-12 mt-0">
      <input type="text" name="current_address_en" class="form-control" value="<?= getValues("current_address_en") ?>" placeholder="Current Address (25/55 SomeCondo Fl. 7 Building 1...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_province_en" class="form-control" value="<?= getValues("current_address_province_en") ?>" placeholder="Province (Pathum Thani/Bangkok...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_district_en" class="form-control" value="<?= getValues("current_address_district_en") ?>" placeholder="District (Sam Khok/Phaya Thai...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_subdistrict_en" class="form-control" value="<?= getValues("current_address_subdistrict_en") ?>" placeholder="Subdistrict (Bang Krabue/Samsen Nai...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="current_address_postcode_en" class="form-control" value="<?= getValues("current_address_postcode_en") ?>" placeholder="Postal Code (12345...)" required/>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12"><h5>Permanant Address (TH) <sup class="text-danger">*</sup></h5></div>
    <div class="col-12 mt-0">
      <input type="text" name="permanant_address_th" class="form-control" value="<?= getValues("permanant_address_th") ?>" placeholder="Permanant Address (777 ถ.เพชรเกษม...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_province_th" class="form-control" value="<?= getValues("permanant_address_province_th") ?>" placeholder="Province (นครปฐม/กรุงเทพฯ...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_district_th" class="form-control" value="<?= getValues("permanant_address_district_th") ?>" placeholder="District (นครชัยศรี/บางแค...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_subdistrict_th" class="form-control" value="<?= getValues("permanant_address_subdistrict_th") ?>" placeholder="Subdistrict (ท่าตำหนัก/บางแคเหนือ...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_postcode_th" class="form-control" value="<?= getValues("permanant_address_postcode_th") ?>" placeholder="Postal Code (12345...)" required/>
    </div>
  </div>
</div>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12"><h5>Permanant Address (EN) <sup class="text-danger">*</sup></h5></div>
    <div class="col-12 mt-0">
      <input type="text" name="permanant_address_en" class="form-control" value="<?= getValues("permanant_address_en") ?>" placeholder="Permanant Address (777 Petchkasem Rd...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_province_en" class="form-control" value="<?= getValues("permanant_address_province_en") ?>" placeholder="Province (Nakhon Pathom/Bangkok...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_district_en" class="form-control" value="<?= getValues("permanant_address_district_en") ?>" placeholder="District (Nakhon Chai Si/Bang Khae...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_subdistrict_en" class="form-control" value="<?= getValues("permanant_address_subdistrict_en") ?>" placeholder="Subdistrict (Tha Tamnak/Bang Khae Nuea...)" required/>
    </div>
    <div class="col-md-3 col-sm-6">
      <input type="text" name="permanant_address_postcode_en" class="form-control" value="<?= getValues("permanant_address_postcode_en") ?>" placeholder="Postal Code (12345...)" required/>
    </div>
  </div>
</div>
<h5 class="mt-5">Personal Details</h5>
<hr>
<div class="mb-3">
  <div class="row g-3 align-items-center">
    <div class="col-12">
      <h5>National ID Card</h5>
      <input type="text" name="national_card_id" class="form-control" value="<?= getValues("national_card_id") ?>" placeholder="Card ID (XXXXX-XXXXX-XXX)" required/>
    </div>
    <div class="col-12">
      <h5>Issued At</h5>
    </div>
    <div class="col-6 mt-0">
      <input type="text" name="national_card_issued_district" class="form-control" value="<?= getValues("national_card_issued_district") ?>" placeholder="District (บางกรวย...)" required/>
    </div>
    <div class="col-6 mt-0">
      <input type="text" name="national_card_issued_province" class="form-control" value="<?= getValues("national_card_issued_province") ?>" placeholder="Province (นนทบุรี...)" required/>
    </div>
    <div class="col-6">
      <h4>Issued</h4>
      <input type="date" name="national_card_issued_date" class="form-control" value="<?= getValues("national_card_issued_date") ?>" placeholder="Issued Date" id="national_card_issued_date" required/>
    </div>
    <div class="col-6">
      <h4>Expired</h4>
      <input type="date" name="national_card_issued_expired" class="form-control" value="<?= getValues("national_card_issued_expired") ?>" placeholder="Expired Date" id="national_card_issued_expired" required/>
    </div>
    <div class="col-12">
      <h5>Tax ID Card</h5>
      <input type="text" name="tax_card_id" class="form-control" value="<?= getValues("tax_card_id") ?>" placeholder="Tax ID Card" required/>
    </div>
    <div id="marital_status" class="row g-3 align-items-center">
      <div class="col-12">
        <h5>Marital Status</h5>
      </div>
      <div class="col-md-2 mt-0">
        <div class="form-check" onclick="validate()">
          <input type="radio" class="form-check-input" name="marital_status" id="single_marital_status" value="single" <?php if (getValues("marital_status") === null || getValues("marital_status") === "single") { ?>checked<?php } ?>>
          <label class="form-check-label" for="single_marital_status">
            Single
          </label>
        </div>
      </div>
      <div class="col-md-2 mt-0">
        <div class="form-check" onclick="validate()">
          <input type="radio" class="form-check-input" name="marital_status" id="married_marital_status" value="married" <?php if (getValues("marital_status") === "married") { ?>checked<?php } ?>>
          <label class="form-check-label" for="married_marital_status">
            Married
          </label>
        </div>
      </div>
      <div class="col-md-2 mt-0">
        <div class="form-check" onclick="validate()">
          <input type="radio" class="form-check-input" name="marital_status" id="divorced_marital_status" value="divorced"  <?php if (getValues("marital_status") === "divorced") { ?>checked<?php } ?>>
          <label class="form-check-label" for="divorced_marital_status">
            Divorced
          </label>
        </div>
      </div>
      <div class="col-md-2 mt-0">
        <div class="form-check" onclick="validate()">
          <input type="radio" class="form-check-input" name="marital_status" id="widowed_marital_status" value="widowed" <?php if (getValues("marital_status") === "widowed") { ?>checked<?php } ?>>
          <label class="form-check-label" for="widowed_marital_status">
            Widowed
          </label>
        </div>
      </div>
      <div class="col-md-2 mt-0">
        <div class="form-check" onclick="validate()">
          <input type="radio" class="form-check-input" name="marital_status" id="separated_marital_status" value="separated" <?php if (getValues("marital_status") === "separated") { ?>checked<?php } ?> >
          <label class="form-check-label" for="separated_marital_status">
            Separated
          </label>
        </div>
      </div>
    </div>
    <div id="if_married" class="row g-3 align-items-center">
      <div class="col-12">
        <h4>If Married</h4>
      </div>
      <div class="col-auto mt-0">
        <div class="form-check">
          <input type="radio" class="form-check-input" name="if_married" id="registered_if_married" value="registered" <?php if (getValues("if_married") === null || getValues("if_married") === "registered") { ?>checked<?php } ?>>
          <label class="form-check-label" for="registered_if_married">
            Registered
          </label>
        </div>
      </div>
      <div class="col-auto mt-0">
        <div class="form-check">
          <input type="radio" class="form-check-input" name="if_married" id="non_registered_if_married" value="non_registered" <?php if (getValues("if_married") === "non_registered") { ?>checked<?php } ?>>
          <label class="form-check-label" for="non_registered_if_married">
            Non-Registered
          </label>
        </div>
      </div>
      <input style="display: none;" type="radio" name="if_married" id="single_if_married" value="single" <?php if (getValues("if_married") === "single") { ?>checked<?php } ?>>
    </div>
  </div>
</div>
<?php
        break;
      case "healthy_details":
?>

<?php
        break;
      default:
        echo "<h3 class='p-3'>Error</h3>";
    }
  }
?>
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <div class="card-header pb-0">
        <h4 class="mb-3">Create New Employee</h4>
        <h5> 
          <?php
          if (isset($_POST["next-btn"])) {
            switch ($_POST["next-btn"]) {
              case "healthy_details":
                echo "Healthy Details";
                break;
              default:
                echo "Error";
            }
          }
          else {
            echo "Personal Details";
          }
          ?>
        </h5>
        <hr>
      </div>
      <div class="card-body px-0 pt-0 pb-2">
        <div class="p-4">
          <form method="post" action="manage_user_add.php">
          <?php
            if ($_POST) {
              foreach ($_POST as $name => $value) {
                if ($name !== "next-btn") {
            ?>
              <input type="hidden" value="<?= $value ?>" name="<?= $name ?>">
            <?php
                }
              }
            }

            if (isset($_POST["next-btn"])) {
              element($_POST["next-btn"]);
            }
            else {
              element("basics");
            }
          ?>
            <div class="text-end">
              <button
              <?php
              if (isset($_POST["next-btn"])) {
              ?>
              type="submit" name="prev-btn"
              <?php
              } else {
              ?>
              onclick="history.back()"
              <?php
              }
              ?>
              class="btn bg-gradient-danger my-4 mb-2">
              <?php
                if (isset($_POST["next-btn"])) {
                  echo '<i class="fa fa-chevron-left pe-1"></i> Back';
                }
                else {
                  echo "Cancel";
                }
              ?>
              </button>
              <button type="submit" name="next-btn" value="<?php
                if (isset($_POST["next-btn"])) {
                  switch ($_POST["next-btn"]) {
                    case "healthy_details":
                      echo "education_details";
                      break;
                    case "education_details":
                      echo "employment_details";
                      break;
                    default:
                      echo "Error";
                  }
                }
                else {
                  echo "healthy_details";
                }
              ?>" class="btn bg-gradient-success my-4 mb-2">Next <i class="fa fa-chevron-right ps-1"></i></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  flatpickr("#dob", {
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
    maxDate: "today",
  });

  flatpickr("#national_card_issued_date", {
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
    maxDate: "today",
  });

  flatpickr("#national_card_issued_expired", {
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
    minDate: "today",
  });
</script>

<script>
  function validate() {
    if (document.getElementById('single_marital_status').checked) {
      document.getElementById("if_married").style.display = "none"
      document.getElementById('registered_if_married').removeAttribute("checked")
      document.getElementById('single_if_married').setAttribute("checked", "")
    } else {
      document.getElementById("if_married").style.display = "flex"
    }
  }

  window.onload = validate()
</script>
<?php
  include("includes/settings.php");
  include("includes/footer.php");
?>
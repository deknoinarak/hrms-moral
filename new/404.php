<?php
  include("assets/includes/header.php");
?>
<div class="d-flex flex-column justify-content-center align-items-center">
  <h1 class="text-xl">Not Found</h1>
  <button class="btn btn-secondary" onclick="history.back();">Go Back</button>
</div>
<?php
  include("assets/includes/footer.php");
?>
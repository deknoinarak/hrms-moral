      <div class="container">
        <footer class="row row-cols-1 row-cols-sm-2 row-cols-md-5 py-5 my-5 border-top">
          <div class="col mb-3">
            <a href="https://moralcenter.or.th" class="d-flex align-items-center mb-3 link-dark text-decoration-none">
              <img src="/new/assets/svg/mclogo.svg" alt="MC-LOGO" width="60">
            </a>
            <p class="text-muted">© <?= date("Y") ?></p>
          </div>

          <div class="col mb-3"></div>
          <div class="col mb-3"></div>
          <div class="col mb-3"></div>

          <div class="col mb-3">
            <h5>Moral HRMS</h5>
            <ul class="nav flex-column">
              <li class="nav-item mb-2"><a href="https://github.com/Deknoinarak/hrms-moral" class="nav-link p-0 text-muted">Github</a></li>
              <li class="nav-item mb-2"><a href="https://moralcenter.or.th" class="nav-link p-0 text-muted">Moralcenter</a></li>
              <li class="nav-item mb-2"><a href="https://moralcenter.or.th/eservice" class="nav-link p-0 text-muted">Moral E-Service</a></li>
              <li class="nav-item mb-2"><a href="credit.php" class="nav-link p-0 text-muted">Credit</a></li>
            </ul>
          </div>
        </footer>
      </div>
    </div>
  </main>
</body>

</html>
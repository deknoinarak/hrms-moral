<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="/new/assets/svg/mclogo.svg" type="image/svg+xml">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="/new/assets/scss/main.css">
  <script src="/new/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
  <title>Moral HRMS</title>
</head>

<body>
  <div class="sticky-top">
    <nav class="navbar navbar-dark navbar-expand-lg bg-gradient-dark">
      <div class="container-fluid px-5 py-2">
        <a class="d-flex align-items-center navbar-brand me-3" href="/new">
          <img src="/new/assets/svg/mclogo.svg" alt="MC-LOGO" width="28" class="d-inline-block align-text-top me-3">
          Moral HRMS
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="employeesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Employees
              </a>
              <ul class="dropdown-menu" aria-labelledby="employeesDropdown">
                <li><a class="dropdown-item" href="manage_employees.php">Manage Employees</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="leaveDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Leave
              </a>
              <ul class="dropdown-menu" aria-labelledby="leaveDropdown">
                <li><a class="dropdown-item" href="manage_leave.php">My Leave</a></li>
                <li><a class="dropdown-item" href="manage_leave.php&view=request">Leave Request</a></li>
              </ul>
            </li>
          </ul>
          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>

    <nav class="navbar navbar-expand-lg bg-light">
      <div class="container-fluid px-5 py-2">
        <div class="row align-items-center w-100">
          <div class="col">
            <span class="navbar-brand">
              <i class="fa-solid fa-chart-line me-2"></i>
              Dashboard
            </span>
          </div>
          <div class="col d-flex justify-content-center">
            <div class="collapse navbar-collapse" id="navbar">
              <ul class="navbar-nav mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Dropdown
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a class="nav-link disabled">Disabled</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col"></div>
        </div>
      </div>
    </nav>
  </div>

  <main>
    <div class="conatiner-fluid px-5 py-3">
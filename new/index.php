<?php
  include("assets/includes/header.php");
?>
<div class="mt-5 container-sm">
  <div class="p-3 mb-2 bg-gradient-primary text-white">.bg-gradient-primary</div>
  <div class="p-3 mb-2 bg-gradient-secondary text-white">.bg-gradient-secondary</div>
  <div class="p-3 mb-2 bg-gradient-success text-white">.bg-gradient-success</div>
  <div class="p-3 mb-2 bg-gradient-danger text-white">.bg-gradient-danger</div>
  <div class="p-3 mb-2 bg-gradient-warning text-dark">.bg-gradient-warning</div>
  <div class="p-3 mb-2 bg-gradient-info text-dark">.bg-gradient-info</div>
  <div class="p-3 mb-2 bg-gradient-light text-dark">.bg-gradient-light</div>
  <div class="p-3 mb-2 bg-gradient-dark text-white">.bg-gradient-dark</div>
  <div class="p-3 mb-2 bg-gradient-body text-dark">.bg-gradient-body</div>
  <div class="p-3 mb-2 bg-gradient-white text-dark">.bg-gradient-white</div>
  <div class="p-3 mb-2 bg-gradient-transparent text-dark">.bg-gradient-transparent</div>
</div>

<div class="mt-5 container-sm">
  <button type="button" class="btn btn-primary">Primary</button>
  <button type="button" class="btn btn-secondary">Secondary</button>
  <button type="button" class="btn btn-success">Success</button>
  <button type="button" class="btn btn-danger">Danger</button>
  <button type="button" class="btn btn-warning">Warning</button>
  <button type="button" class="btn btn-info">Info</button>
  <button type="button" class="btn btn-light">Light</button>
  <button type="button" class="btn btn-dark">Dark</button>
  <button type="button" class="btn btn-link">Link</button>
</div>

<div class="mt-5 container-sm">
  <button type="button" class="btn btn-primary bg-gradient-primary">Gradient Primary</button>
  <button type="button" class="btn btn-secondary bg-gradient-secondary">Gradient Secondary</button>
  <button type="button" class="btn btn-success bg-gradient-success">Gradient Success</button>
  <button type="button" class="btn btn-danger bg-gradient-danger">Gradient Danger</button>
  <button type="button" class="btn btn-warning bg-gradient-warning">Gradient Warning</button>
  <button type="button" class="btn btn-info bg-gradient-info">Gradient Info</button>
  <button type="button" class="btn btn-light bg-gradient-light">Gradient Light</button>
  <button type="button" class="btn btn-dark bg-gradient-dark">Gradient Dark</button>
  <button type="button" class="btn btn-link bg-gradient-link">Gradient Link</button>
</div>
<?php
  include("assets/includes/footer.php");
?>
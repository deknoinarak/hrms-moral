<?php
    session_start();

    if (isset($_POST["create_employee"])) {
        require "databases/db_connect.php";

        $prefix = $_POST["name_prefix"];
        $first_name = $_POST["name_first"];
        $last_name = $_POST["name_last"];
        $email = $_POST["email"];

        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $password = implode($pass);
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $img = 'https://cdn-icons-png.flaticon.com/512/3011/3011270.png';

        $sql = "INSERT INTO hrms_accounts (img, prefix, first_name, last_name, email, password) 
        VALUES ('$img', '$prefix', '$first_name', '$last_name', '$email', '$hashed_password')";

        if (mysqli_query($conn, $sql)) {
            $_SESSION["success"] = "Create Employee Successfully!";
            mysqli_close($conn);
            header("Location: ../manage_employees.php");
        }
        else {
            $_SESSION["error"] = "Error: " . $sql . "<br>" . mysqli_error($conn);
            mysqli_close($conn);
            header("Location: ../manage_employees.php");
        }
    }
    else if (isset($_POST["edit_account_settings"])) {
        require "databases/db_connect.php";

        $ID = $_POST["id"];
        $email = $_POST["email"];
        $password = $_POST["password"];

        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $sql = "UPDATE hrms_accounts SET email='$email', password='$hashed_password'";

        if (mysqli_query($conn, $sql)) {
            print_r(["status" => "success", "details" => "update_success"]);
            mysqli_close($conn);
        }
        else {
            print_r(["status" => "unsuccess", "details" => mysqli_error($conn)]);
            mysqli_close($conn);
        }
    }
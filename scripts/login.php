<?php
    session_start();

    function login($val) {
        if (isset($val["login"])) {
            require "databases/db_connect.php";

            $sql = "SELECT password FROM hrms_accounts WHERE email='$val[email]'";
            $result = mysqli_query($conn, $sql);

            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);

                if (password_verify($val["password"], $row["password"])) {
                    print_r(["status" => "success", "details" => "password_verified"]);
                }
                else print_r(["status" => "unsuccess", "details" => "password_unverified"]);
            }
            else {
                print_r(["status" => "unsuccess", "details" => "user_not_exists"]);
            }
        }
    }

    if (isset($_POST["login"])) {
        login($_POST);
    }